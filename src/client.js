var socket = io('http://localhost:8080/');
socket.on('error', function () {
	console.log('Failed to connect');
});

/**
 * @type {Array}
 */
var users = [];

/**
 * @type {Object}
 */
var key = new triplesec.Buffer('12345678');

/**
 * @type {Object}
 */
var encryptor = new triplesec.Encryptor({ key: key });

/**
 * @type {Object}
 */
var decryptor = new triplesec.Decryptor({ key: key });

/**
 * @type {number}
 */
var messageCount = 0;

/**
 * @type {number}
 */
var resaltThreshold = Math.floor(Math.random() * 50) + 1;

/**
 * @type {number}
 */
var resaltTimeout = setTimeout(resalt, resaltThreshold * 12000);

/**
 * Resalt derived keys.
 */
function resalt() {
	clearTimeout(resaltTimeout);

	encryptor.resalt({}, function () {
		messageCount = 0;
		resaltThreshold = Math.floor(Math.random() * 50) + 1;
		resaltTimeout = setTimeout(resalt, resaltThreshold * 12000);
	});
}

socket.on('connect', function () {
	console.log('Connection established');

	encryptor.run({
		data: new triplesec.Buffer('Hello world')
	}, function (error, buffer) {
		if (!error) {
			socket.emit('message', buffer.toString('hex'));

			if (++messageCount >= resaltThreshold) {
				resalt();
			}
		}
	});
});

socket.on('disconnect', function () {
	console.log('Connection lost');
});

/**
 * Iterate over each element using a callback.
 * @param {Array} array
 * @callback callback
 */
function forEach(array, callback) {
	for (var i = 0; i < array.length; i++) {
		callback(array[i]);
	};
}

/**
 * Filter an array using a callback.
 *
 * @param {Array} array
 * @callback callback
 * @returns {Array}
 */
function filter(array, callback) {
	var newArray = [];

	for (var i = 0; i < array.length; i++) {
		var element = array[i];

		if (callback(element)) {
			newArray.push(element);
		}
	}

	return newArray;
}

/**
 * Find in array using a callback.
 *
 * @param {Array} array
 * @callback callback
 * @returns {}
 */
function find(array, callback) {
	for (var i = 0; i < array.length; i++) {
		var element = array[i];

		if (callback(element)) {
			return element;
		}
	}

	return false;
}

socket.on('status', function (response) {
	if (response.type === 'connect') {
		return forEach(response.data, function (user) {
			users.push(user);
		});
	}

	users = filter(users, function (user) {
		return user.id !== response.data.id;
	});
});

socket.on('message', function (response) {
	var user = find(users, function (user) {
		return user.id === response.from;
	});

	console.log(user);

	decryptor.run({
		data: new triplesec.Buffer(response.data, 'hex')
	}, function (error, buffer) {
		if (!error) {
			console.log(buffer.toString());
		}
	});
});