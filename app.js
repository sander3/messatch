var app = require('http').createServer(handler);
var io = require('socket.io')(app);
var fs = require('fs');

app.listen(8080);

function handler(req, res) {
	var filename = (req.url === '/' ? '/index.html' : req.url);
	fs.readFile(__dirname + filename, function (err, data) {
		if (err) {
			res.writeHead(500);
			return res.end('Error loading ' + req.url);
		}

		res.writeHead(200);
		return res.end(data);
	});
}

/**
 * @type {Array}
 */
var users = [];

/**
 * Format response data.
 * @param {string} type
 * @param {Array} data
 * @returns {Object}
 */
function responseFormatter(type, data) {
	return {
		type: type,
		data: data
	};
}

/**
 * Filter an array using a callback.
 *
 * @param {Array} array
 * @callback callback
 * @returns {Array}
 */
function filter(array, callback) {
	var newArray = [];

	for (var i = 0; i < array.length; i++) {
		var element = array[i];

		if (callback(element)) {
			newArray.push(element);
		}
	}

	return newArray;
}

io.on('connection', function (socket) {
	if (users.length !== 0) {
		socket.emit('status', responseFormatter('connect', users));
	}

	var user = {
		id: socket.id,
		username: socket.username,
		remoteAddress: socket.request.connection.remoteAddress
	};

	users.push(user);
	socket.broadcast.emit('status', responseFormatter('connect', [user]));

	socket.on('disconnect', function () {
		users = filter(users, function (user) {
			return user.id !== socket.id;
		});

		socket.broadcast.emit('status', responseFormatter('disconnect', user));
	});

	socket.on('message', function (request) {
		socket.broadcast.emit('message', {
			from: socket.id,
			data: request
		});
	});
});