module.exports = function (grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		bower_concat: {
			all: {
				dest: 'src/bower_components.js'
			}
		},
		concat: {
			js: {
				src: [
					'node_modules/socket.io-client/socket.io.js',
					'src/*.js',
					'!src/client.js',
					'src/client.js'
				],
				dest: 'dist/<%= pkg.name %>.js'
			}
		},
		watch: {
			options: {
				spawn: false
			},
			js: {
				files: '<%= concat.js.src %>',
				tasks: 'concat'
			}
		}
	});

	grunt.task.loadNpmTasks('grunt-bower-concat');
	grunt.task.loadNpmTasks('grunt-contrib-concat');
	grunt.task.loadNpmTasks('grunt-contrib-watch');

	grunt.task.registerTask('default', ['bower_concat', 'concat']);
}